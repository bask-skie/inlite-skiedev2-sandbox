public class QuoteLineController {
  public static void createQuoteLine(List<SBQQ__QuoteLine__c> quoteLineList){
      Set<String> quoteIdSet = new Set<String>();
      Set<String> quoteGroupIdSet = new Set<String>();
      List<Read_Only_Quote_Line__c> readOnlyQuoteLineList = new List<Read_Only_Quote_Line__c>();
      Map<String, Read_Only_Quote_Line__c> readOnlyQuoteLineMap = new Map<String, Read_Only_Quote_Line__c>();
      Map<String, String> readOnlyQuoteMap = new Map<String, String>();
      Map<String, String> readOnlyQuoteGroupMap = new Map<String, String>();
      
      for(SBQQ__QuoteLine__c quoteLine : quoteLineList){
          if(quoteLine.SBQQ__Quote__c != null){
              quoteIdSet.add(quoteLine.SBQQ__Quote__c);
          }
          if(quoteLine.SBQQ__Group__c != null) {
              quoteGroupIdSet.add(quoteLine.SBQQ__Group__c);
          }
      }
      
      List<SBQQ__Quote__c> quoteList = [Select Id, Read_Only_Quote__c From SBQQ__Quote__c Where Id in :quoteIdSet];
      for(SBQQ__Quote__c quote : quoteList){
          readOnlyQuoteMap.put(quote.Id, quote.Read_Only_Quote__c);
      }
      
      List<SBQQ__QuoteLineGroup__c> quoteGroupList = [Select Id, Read_Only_Quote_Group__c From SBQQ__QuoteLineGroup__c Where Id in :quoteGroupIdSet];
      for(SBQQ__QuoteLineGroup__c qGroup: quoteGroupList) {
          readOnlyQuoteGroupMap.put(qGroup.Id, qGroup.Read_Only_Quote_Group__c);
      }
      
      for(SBQQ__QuoteLine__c quoteLine: quoteLineList){
          Read_Only_Quote_Line__c readOnlyQuoteLine = new Read_Only_Quote_Line__c();
          readOnlyQuoteLine.Quote_Line__c = quoteLine.Id;
          readOnlyQuoteLine.Read_Only_Quote__c = readOnlyQuoteMap.get(quoteLine.SBQQ__Quote__c);
          readOnlyQuoteLine.Read_Only_Quote_Group__c = readOnlyQuoteGroupMap.get(quoteLine.SBQQ__Group__c);
          readOnlyQuoteLineList.add(readOnlyQuoteLine);
          readOnlyQuoteLineMap.put(quoteLine.Id, readOnlyQuoteLine);
      }
      if(readOnlyQuoteLineList.size() > 0){
          insert readOnlyQuoteLineList;
          
          List<SBQQ__QuoteLine__c> newQuoteLineList= [Select Id, Name, Read_Only_Quote_Line__c, SBQQ__Quantity__c From SBQQ__QuoteLine__c Where Id In : quoteLineList];
          
          for(SBQQ__QuoteLine__c quoteLine: newQuoteLineList){
              if(readOnlyQuoteLineMap.get(quoteLine.Id) != null){
                  quoteLine.Read_Only_Quote_Line__c = readOnlyQuoteLineMap.get(quoteLine.Id).Id;
              }
          }
          
          if(newQuoteLineList.size() > 0) {
              
              update newQuoteLineList;
          }
      }
    }
    
    public static void deleteQuoteLine(List<SBQQ__QuoteLine__c> quoteLineList){
       
        Set<String> readOnlyQuoteLineId = new Set<String>();
        for(SBQQ__QuoteLine__c quoteLine: quoteLineList){
            readOnlyQuoteLineId.add(quoteLine.Read_Only_Quote_Line__c);
        }
        List<Read_Only_Quote_Line__c> readOnlyQuoteLineList = [Select Id From Read_Only_Quote_Line__c Where Id in: readOnlyQuoteLineId];
        if(readOnlyQuoteLineList.size() > 0) {
            delete readOnlyQuoteLineList;
        }
        
    } 
    
     public static void updateQuoteLine(List<SBQQ__QuoteLine__c> quoteLineList){
        
        Set<String> quoteLineGroupId = new Set<String>();
        Set<String> quoteLineId = new Set<String>();
        Map<String, Read_Only_Quote_Line__c> readOnlyQuoteLineMap = new Map<String, Read_Only_Quote_Line__c>();
        List<Read_Only_Quote_Line__c> updateReadOnlyQuoteLineList = new List<Read_Only_Quote_Line__c>();
         
        for(SBQQ__QuoteLine__c quoteLine : quoteLineList){
            if(quoteLine.SBQQ__Group__c != null){
                quoteLineGroupId.add(quoteLine.SBQQ__Group__c);
            }
            quoteLineId.add(quoteLine.Id);
        }
         
         List<SBQQ__QuoteLineGroup__c> quoteLineGroupList = [Select Id, Read_Only_Quote_Group__c, SBQQ__Quote__c
                                                             From SBQQ__QuoteLineGroup__c 
                                                             Where Id in : quoteLineGroupId];
         List<Read_Only_Quote_Line__c> readOnlyQuoteLineList = [Select Id, Quote_Line__r.SBQQ__Quote__c
                                                                From Read_Only_Quote_Line__c 
                                                                Where Quote_Line__c in : quoteLineId];
         
         Map<String, SBQQ__QuoteLineGroup__c> mapQuoteLineGroup = new Map<String, SBQQ__QuoteLineGroup__c>(quoteLineGroupList);
         Map<String, Read_Only_Quote_Line__c> mapReadOnlyQuoteLine = new Map<String, Read_Only_Quote_Line__c>(readOnlyQuoteLineList);
         for(SBQQ__QuoteLine__c quoteLine: quoteLineList) {
             
             if(mapReadOnlyQuoteLine.get(quoteLine.Read_Only_Quote_Line__c) != null) {
                 if(quoteLine.SBQQ__Group__c != null) {
                     
                     String readOnlyQuoteGroupId = mapQuoteLineGroup.get(quoteLine.SBQQ__Group__c).Read_Only_Quote_Group__c;
                     mapReadOnlyQuoteLine.get(quoteLine.Read_Only_Quote_Line__c).Read_Only_Quote_Group__c = readOnlyQuoteGroupId;
                     
                 }else {
                     mapReadOnlyQuoteLine.get(quoteLine.Read_Only_Quote_Line__c).Read_Only_Quote_Group__c = null;
                 }
                 updateReadOnlyQuoteLineList.add(mapReadOnlyQuoteLine.get(quoteLine.Read_Only_Quote_Line__c)); 
             }          
         } 
         
         if(updateReadOnlyQuoteLineList.size() > 0){
             update updateReadOnlyQuoteLineList;
         }
    }
    
    public static void checkingRoqlBeforeUpdate(List<SBQQ__QuoteLine__c> quoteLineList){
                    
        List<Read_Only_Quote_Line__c> readQuoteLineList = [Select Id, Quote_Line__c From Read_Only_Quote_Line__c Where Quote_Line__c In :quoteLineList];
        Map<String, String> readQuoteLineMap = new Map<String, String>();
        
        for(Read_Only_Quote_Line__c q : readQuoteLineList) {
            if(q.Quote_Line__c != null) {
                readQuoteLineMap.put(q.Quote_Line__c, q.Id);
            }
        }
            
        for(SBQQ__QuoteLine__c q: quoteLineList){
            if(readQuoteLineMap.get(q.Id) != null) {
                q.Read_Only_Quote_Line__c = readQuoteLineMap.get(q.Id);
            }
        }     
    }
}