public class CustomDataloadController {
    
    private static final String COMPONENT = 'Components';
    private static final String OPTION = 'Options';
    private static final String ACCESSORY = 'Accessories';
    private static final String PRODUCT_RULE_NAME = 'Alert if no Components';
    private static final String NEW_ZEALAND_PRICE_BOOK = 'New Zealand Price Book';
    private static final String AU_PREFIX = 'AU_';
    private static final String NZ_PREFIX = 'NZ_';
    private static final String COMPONENT_PREFIX = 'COMPONENT_';
    private static final String OPTION_PREFIX = 'OPTION_';
    private static final String ACCESSORY_PREFIX = 'ACCESSORY_';

    public static void populateExistingProducts(List<Dataload__c> newDataloadList){
        Set<String> productIdSet = new Set<String>();
        Map<String, Product2> productMap = new Map<String, Product2>();
        
        //setup data
        for(Dataload__c dataload: newDataloadList) {
            if(dataload.ProductID__c != null) {
                productIdSet.add(dataload.ProductID__c);
            }
        }
        
        //given data 
        List<Product2> products = [Select Id, ProductID__c From Product2 Where ProductID__c In: productIdSet];
        for(Product2 product: products) {
            if(product.ProductID__c != null) {
                productMap.put(product.ProductID__c, product);
            }
        }
        
        //execute process for checking existing product
        for(Dataload__c dataload: newDataloadList) {
            Product2 product = productMap.get(dataload.ProductID__c);
            if(product != null) {
                dataload.Product__c = product.Id;
                dataload.Existing_Product__c = true;
            }else {
                dataload.No_Existing_Product__c = true;
            }
        }
    }
    
    public static void createNewProducts(List<Dataload__c> newDataloadList) {
        List<Product2> newProducts = new List<Product2>();
        Map<String , Product2> newProductMap = new Map<String, Product2>();
        //Map data to create a new Product
        for(Dataload__c dataload: newDataloadList) {
            System.debug('ProductId__c: '+dataload.ProductId__c);
            if(dataload.No_Existing_Product__c && dataload.ProductId__c != null) {
                Product2 product = new Product2();
                product.IsActive = true;
                product.ProductID__c = dataload.ProductId__c;
                product.Name = dataload.ProductId__c;
                product.ProductCode = dataload.Product_Code__c;
                product.Product_Type__c = dataload.Product_Type__c;
                product.Family_Name__c = dataload.Family_Name__c;
                product.Description = dataload.Product_Description__c;
                product.IP_Rating__c = dataload.IP_Rating__c;
                product.IK_Rating__c = dataload.IK_Rating__c;
                product.Light_Source__c = dataload.Light_Source__c;
                product.Light_Source_Base__c = dataload.Light_Source_Base__c;
                product.Voltage__c = dataload.Voltage__c;
                product.Current__c = dataload.Current__c;
                product.Wattage__c = dataload.Wattage__c;
                product.System_Power__c = dataload.System_Power__c;
                product.Lumen_Package__c = dataload.Lumen_Package__c;
                product.Luminous_Flux__c = dataload.Luminous_Flux__c;
                product.Colour_Temp__c = dataload.Colour_Temperature__c;
                product.CRI__c = dataload.CRI__c;
                product.Chromacity_Tolerance__c = dataload.Chromacity_Tolerance__c;
                product.Beam_Angle__c = dataload.Beam_Angle__c;
                product.Distribution_Type__c = dataload.Distribution_Type__c;
                product.Control_Gear__c = dataload.Control_Gear__c;
                product.Control_Gear_Detail__c = dataload.Control_Gear_Detail__c;
                product.Product_Application_Type__c = dataload.Product_Application_Type__c;
                product.Application_Type__c = dataload.Application_Type__c;
                product.Brands__c = dataload.Manufacturer__c;
                
                product.SBQQ__ConfigurationType__c = 'Allowed';
                product.SBQQ__ConfigurationEvent__c = 'Always';
                product.SBQQ__OptionLayout__c = 'Sections';
                product.SBQQ__OptionSelectionMethod__c = 'Click';
                product.SBQQ__PriceEditable__c = true;
                product.SBQQ__PricingMethod__c = 'List';
                product.SBQQ__PricingMethodEditable__c = true;
                
                newProducts.add(product);
            } 
        }
        if(newProducts.size() > 0) {
            insert newProducts;
        }
        
    }
    
    public static void manageCustomDataload(List<Dataload__c> dataloadList) {
        
        List<PricebookEntry> auPricebookEntryList = new List<PricebookEntry>();
        List<PricebookEntry> nzPricebookEntryList = new List<PricebookEntry>();
        List<SBQQ__Cost__c> auCostList = new List<SBQQ__Cost__c>();
        List<SBQQ__Cost__c> nzCostList = new List<SBQQ__Cost__c>();
        List<SBQQ__ProductFeature__c> productFeatureList = new List<SBQQ__ProductFeature__c>();
        List<SBQQ__ConfigurationRule__c> configurationRuleList = new List<SBQQ__ConfigurationRule__c>();
        
        List<SBQQ__ProductOption__c> componentProductOptionList = new List<SBQQ__ProductOption__c>();
        List<SBQQ__ProductOption__c> optionProductOptionList= new List<SBQQ__ProductOption__c>();
        List<SBQQ__ProductOption__c> accessoryProductOptionList = new List<SBQQ__ProductOption__c>();
        
        Set<String> productIdSet = new Set<String>();
        Set<String> productComponentIdSet = new Set<String>();
        Set<String> productOptionIdSet = new Set<String>();
        Set<String> productAccessoryIdSet = new Set<String>();
        
        Map<String, Product2> productMap = new Map<String, Product2>();
        Map<String, Product2> productComponentMap = new Map<String, Product2>();
        Map<String, Product2> productOptionMap = new Map<String, Product2>();
        Map<String, Product2> productAccessoryMap = new Map<String, Product2>();
        
        Id stdPricebookEntryId = CustomDataloadAssembler.getStandardPricebookId();
        Id nzPricebookEntryId = CustomDataloadAssembler.getPricebookIdByName(NEW_ZEALAND_PRICE_BOOK);
        Id productRuleId = CustomDataloadAssembler.getProductRuleIdByName(PRODUCT_RULE_NAME);
        
        
        for(Dataload__c dataload: dataloadList) {
            //given productId for MAP
            if(dataload.ProductId__c != null) {
                productIdSet.add(dataload.ProductId__c);
            }
            
            if(dataload.Associated_Products_Components__c != null) {
                String[] associatedComponents = ((String) dataload.Associated_Products_Components__c).split(',');
                for(String productId: associatedComponents) {
                    productComponentIdSet.add(productId.trim());
                }
            }
            
            if(dataload.Associated_Products_Options__c != null) {
                String[] associatedOptions = ((String) dataload.Associated_Products_Options__c).split(',');
                for(String productId: associatedOptions) {
                    productOptionIdSet.add(productId.trim());
                }
            }
            
            if(dataload.Associated_Products_Accessories__c != null) {
                String[] associatedAccessories = ((String) dataload.Associated_Products_Accessories__c).split(',');
                for(String productId: associatedAccessories) {
                    productAccessoryIdSet.add(productId.trim());
                }
            }
            
        }
        
        for(Product2 product: [SELECT Id, ProductID__c, Product_Type__c 
                               From Product2 
                               Where ProductID__c In: productIdSet]){
            productMap.put(product.ProductID__c, product);
        }
        
        for(Product2 productComponent: [SELECT Id, ProductID__c, Product_Type__c 
                                        From Product2 
                                        Where ProductID__c In: productComponentIdSet]) {
            productComponentMap.put(productComponent.ProductID__c, productComponent);
        }
        
        for(Product2 productOption: [SELECT Id, ProductID__c, Product_Type__c 
                                     From Product2 
                                     Where ProductID__c In: productOptionIdSet]) {
            productOptionMap.put(productOption.ProductID__c, productOption);
        }
        
        for(Product2 productAccessory: [SELECT Id, ProductID__c, Product_Type__c 
                                        From Product2 
                                        Where ProductID__c In: productAccessoryIdSet]) {
            productAccessoryMap.put(productAccessory.ProductID__c, productAccessory);
        }
        
        //NEW CHANGE
        //
        //
        //Given existing AU&NZ List Prices
        //List<PricebookEntry> existingAUPricebookEntryList = [Select Id From PricebookEntry]
     
        
        
        for(Dataload__c dataload: dataloadList) {
            Product2 product = productMap.get(dataload.ProductID__c);
            if(product != null) {
                
                dataload.Product__c = product.Id;
                
                if(dataload.Trade_Price__c != null && stdPricebookEntryId != null) {
                    auPricebookEntryList.add(CustomDataloadAssembler.toBuildPricebookEntry(stdPricebookEntryId, 
                                                                                           dataload.Trade_Price__c, product.Id));
                }
                
                if(dataload.NZ_Trade_Price__c != null && nzPricebookEntryId != null) {
                    nzPricebookEntryList.add(CustomDataloadAssembler.toBuildPricebookEntry(nzPricebookEntryId, 
                                                                                           dataload.NZ_Trade_Price__c, product.Id));
                }
                
                if(dataload.Cost_Price__c != null && stdPricebookEntryId != null) {
                    auCostList.add(CustomDataloadAssembler.toBuildCost(dataload.Cost_Price__c, product.Id, 'AU'));
                }
                
                if(dataload.NZ_Cost_Price__c != null && nzPricebookEntryId != null) {
                    nzCostList.add(CustomDataloadAssembler.toBuildCost(dataload.NZ_Cost_Price__c, product.Id, 'NZ'));
                }
                
                //creating product_feaure for main product only
                if(dataload.Product_Type__c != 'ACCESSORY' && dataload.Product_Type__c != 'COMPONENT' && dataload.Product_Type__c != 'OPTION') {
                	productFeatureList.add(CustomDataloadAssembler.toBuildProductFeature(COMPONENT, 0, null, 1, product.Id));
                    productFeatureList.add(CustomDataloadAssembler.toBuildProductFeature(OPTION, 0, null, 2, product.Id));
                    productFeatureList.add(CustomDataloadAssembler.toBuildProductFeature(ACCESSORY, 0, null, 3, product.Id));
                    
                    if(productRuleId != null) {
                        configurationRuleList.add(CustomDataloadAssembler.toBuildConfigurationRule(productRuleId, product.Id));
                    }
                }
                
            }//end if prod-id != null
        }
        
        Map<String, PricebookEntry> successedPricebookEntryMap = new Map<String, PricebookEntry>();
        Map<String, SBQQ__Cost__c> successedCostMap = new Map<String, SBQQ__Cost__c>();
        Map<String, SBQQ__ProductFeature__c> successedProductFeatureMap = new Map<String, SBQQ__ProductFeature__c>();
        if(auPricebookEntryList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(auPricebookEntryList, false);
            /*for(Database.SaveResult sr: srList) {
                if(sr.isSuccess()) {
                    System.debug('auPricebookEntryList.getId(): '+sr.getId());
                }else {
                    for(Database.Error err : sr.getErrors()) {
                         System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }*/
            
            for(PricebookEntry pricebookEntry :auPricebookEntryList) {
                if(pricebookEntry.id != null){
                    successedPricebookEntryMap.put(AU_PREFIX + pricebookEntry.Product2Id, pricebookEntry);
                }
            }
        }
        
        if(nzPricebookEntryList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(nzPricebookEntryList, false); 
            
            for(PricebookEntry pricebookEntry :nzPricebookEntryList) {
                if(pricebookEntry.id != null){
                    successedPricebookEntryMap.put(NZ_PREFIX + pricebookEntry.Product2Id, pricebookEntry);
                }
            }
        }
        
        if(auCostList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(auCostList, false);
            
            for(SBQQ__Cost__c cost :auCostList) {
                if(cost.id != null){
                    successedCostMap.put(AU_PREFIX + cost.SBQQ__Product__c, cost);
                }
            }
        }
        
        if(nzCostList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(nzCostList, false);
            
            for(SBQQ__Cost__c cost :nzCostList) {
                if(cost.id != null){
                    successedCostMap.put(NZ_PREFIX + cost.SBQQ__Product__c, cost);
                }
            }
        }
        
        if(productFeatureList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(productFeatureList, false);
            
            CustomDataloadAction.setupSuccessedProductFeatureMap(productFeatureList, successedProductFeatureMap,
                                                                COMPONENT_PREFIX, OPTION_PREFIX, ACCESSORY_PREFIX,
                                                                COMPONENT, OPTION, ACCESSORY);  
        }
        
        if(configurationRuleList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(configurationRuleList, false);
        }
        
        //set flag
        CustomDataloadAction.setupSuccessedFlag(dataloadList, productMap, successedPricebookEntryMap, 
                                              successedCostMap, AU_PREFIX, NZ_PREFIX);
        
        //controll related product
        for(Dataload__c dataload: dataloadList) {
            Product2 product = productMap.get(dataload.ProductID__c);
            System.debug('product: '+product + ' flag: '+dataload.IsProcessed__c);
            //manage data for IsProcessed == false
            
            if(product != null && dataload.IsProcessed__c != true) {
                //creating the product_option record with associated product data
                if(dataload.Associated_Products_Components__c != null) {
                    System.debug('Associated_Products_Components__c != null');
                    String[] associatedComponents = ((String) dataload.Associated_Products_Components__c).split(',');
                    
                    CustomDataloadAction.addProductOptionList(dataload, associatedComponents, productComponentMap,
                                                              successedProductFeatureMap, componentProductOptionList,
                                                              COMPONENT_PREFIX, product, COMPONENT);
                }
                //creating the product_option record with associated product data 
                if(dataload.Associated_Products_Options__c != null) {
                    System.debug('Associated_Products_Options__c != null');
                    String[] associatedOptions = ((String) dataload.Associated_Products_Options__c).split(',');
                    
                    CustomDataloadAction.addProductOptionList(dataload, associatedOptions, productOptionMap,
                                                              successedProductFeatureMap, optionProductOptionList,
                                                              OPTION_PREFIX, product, OPTION);
                }
                //creating the product_option record with associated product data
                if(dataload.Associated_Products_Accessories__c != null) {
                    System.debug('Associated_Products_Accessories__c != null');
                    String[] associatedAccessories = ((String) dataload.Associated_Products_Accessories__c).split(',');
                    
                    CustomDataloadAction.addProductOptionList(dataload, associatedAccessories, productAccessoryMap,
                                                              successedProductFeatureMap, accessoryProductOptionList,
                                                              ACCESSORY_PREFIX, product, ACCESSORY);
                }
                dataload.IsProcessed__c = true;
            }
            
            
        }
        
        System.debug('componentProductOptionList.size: '+componentProductOptionList.size());
        if(componentProductOptionList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(componentProductOptionList, false);
        }
        
        System.debug('optionProductOptionList.size: '+optionProductOptionList.size());
        if(optionProductOptionList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(optionProductOptionList, false);
        }
        
        System.debug('accessoryProductOptionList.size: '+accessoryProductOptionList.size());
        if(accessoryProductOptionList.size() > 0) {
            Database.SaveResult[] srList = Database.insert(accessoryProductOptionList, false); 
        }
       
        if(dataloadList.size() > 0) {
            update dataloadList;
        }       
    }
    
}