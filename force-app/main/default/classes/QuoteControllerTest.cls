@isTest
public class QuoteControllerTest {
    
    @isTest static void processCreatedWithValidDataTest() {
        Test.startTest();

		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
         
        User usr = new User(LastName = 'L-TEST',
                           FirstName='F-TEST',
                           Alias = 'TEST',
                           Email = 'quote-test@gmail.com',
                           Username = 'quote-test@gmail.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                          LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Account acc = new Account();
        acc.Name = 'ACC TEST';
        acc.Account_Grade__c = '3';
        acc.Type = 'Specifier';
        acc.Specifier_Type__c = 'End User';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'OPP TEST';
        opp.StageName = 'Quoting';
        opp.CloseDate = System.today() + 1;
        insert opp;

        SBQQ__Quote__c quote = new SBQQ__Quote__c();
        quote.OwnerId = usr.Id;
        quote.SBQQ__Account__c = acc.Id;
        quote.SBQQ__Opportunity2__c = opp.Id;
        insert quote;
        
        Account acc2 = new Account();
        acc2.Name = 'ACC TEST2';
        acc2.Account_Grade__c = '4';
        acc2.Type = 'Specifier';
        acc2.Specifier_Type__c = 'End User';
        insert acc2;
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'OPP TEST2';
        opp2.StageName = 'Quoting';
        opp2.CloseDate = System.today() + 1;
        insert opp2;
        
        quote.SBQQ__Account__c = acc2.Id;
        quote.SBQQ__Opportunity2__c = opp2.Id;
        
        update quote;
        
        delete quote;
        Test.stopTest();
    }

}