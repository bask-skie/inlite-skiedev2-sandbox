@isTest
public class CustomDataloadControllerTest {

    @testSetup static void setup() {
        //setup existing product
        insert buildProduct('DELTALIGHT-9528-ED1-B-W', 'TEST_DELTALIGHT-9528-ED1-B-W', 'LUMINAIRE', '9528 ED1 B-W');
        //setup New Zealand Price Book
        insert buildPricebookByName('New Zealand Price Book');
        //setup product rule
        insert buildProductRule('Alert if no Components', 'Alert', 'Product', 'All', 
                                'No Component has been selected, are you sure you wish to continue', 'Always');
        
    }
    
    private static Product2 buildProduct(String productId, String name, String productType, String productCode) {
        
        return new Product2(IsActive=true, ProductID__c=productId, Name=name, Product_Type__c=productType, ProductCode=productCode);
    }
    
    private static Dataload__c buildDataload(String productId, String productType, String productCode, Decimal tradePrice, 
                                             Decimal costPrice, Decimal nzTradePrice, Decimal nzCostPrice,
                                             String associatedComponents, String associatedOption, String associatedAccessories) {
                                                 
        return new Dataload__c(ProductId__c=productId, Product_Type__c=productType, Product_Code__c=productCode, Trade_Price__c=tradePrice,
                               Cost_Price__c=costPrice, NZ_Trade_Price__c=nzTradePrice, NZ_Cost_Price__c=nzCostPrice,
                               Associated_Products_Components__c=associatedComponents, Associated_Products_Options__c=associatedOption,
                               Associated_Products_Accessories__c=associatedAccessories);
    }
    
    private static Pricebook2 buildPricebookByName(String name) {
        
        return new Pricebook2(Name=name, IsActive=true);
    }
    
    private static SBQQ__ProductRule__c buildProductRule(String name, String ruleType, String scope, String condition, String message, STring event) {
        
        return new SBQQ__ProductRule__c(Name=name, SBQQ__Type__c=ruleType, SBQQ__Active__c=true, SBQQ__Scope__c=scope, SBQQ__ConditionsMet__c=condition,
                                       SBQQ__ErrorMessage__c=message, SBQQ__EvaluationEvent__c=event);
    }
    
    @isTest static void createNewDataloadRecordWithMatchedProductTest() {
        
        Test.startTest();
        insert buildDataload('DELTALIGHT-9528-ED1-B-W', 'LUMINAIRE', '9528 ED1 B-W', 100.50, 60.00, 120.50, 70.00, null, null, null);
        Test.stopTest();
    }
    
    @isTest static void createNewDataloadRecordWithoutMatchedProductTest() {
        
        Test.startTest();
        insert buildDataload('DELTALIGHT-1111-TST-B-K', 'LUMINAIRE', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null);
        Test.stopTest();
    }
    
    @isTest static void executeCustomDataloadScheduleWithValidDataTest() {
        List<Dataload__c> dataloadList = new List<Dataload__c>();
        dataloadList.add(buildDataload('DELTALIGHT-1111-TST-A-1', 'LUMINAIRE', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, 
                                       'DELTALIGHT-1112-TST-A', 'DELTALIGHT-1113-TST-A', 'DELTALIGHT-1114-TST-A'));
        dataloadList.add(buildDataload('DELTALIGHT-1112-TST-A', 'COMPONENT', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        dataloadList.add(buildDataload('DELTALIGHT-1113-TST-A', 'OPTION', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        dataloadList.add(buildDataload('DELTALIGHT-1114-TST-A', 'ACCESSORY', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, null, null, null));
        insert dataloadList;
        
        Test.startTest();
        String sch = '0 0 * * * ?';
        system.schedule('Test Custom Dataload Batch', sch, new CustomDataloadSchedule());
        Test.stopTest();
    }
    
    @isTest static void executeCustomDataloadScheduleWithInvalidDataTest() {
        List<Dataload__c> dataloadList = new List<Dataload__c>();
        dataloadList.add(buildDataload('DELTALIGHT-1111-TST-A-1', 'LUMINAIRE', '1111 TST B-K', 100.50, 60.00, 120.50, 70.00, 
                                       'DELTALIGHT-1112-TST-A', 'DELTALIGHT-1113-TST-A', 'DELTALIGHT-1114-TST-A'));
        insert dataloadList;
        
        Test.startTest();
        String sch = '0 0 * * * ?';
        system.schedule('Test Custom Dataload Batch', sch, new CustomDataloadSchedule());
        Test.stopTest();
    }
}