global class CopyCaseEmailController {

    @InvocableMethod(label='Clone Email From Case Action')
    global static void copyCaseEmailAction(List<CaseEmailRequest> requests) 
    {
        List<CaseEmailResponse> results = new List<CaseEmailResponse>();
        for (CaseEmailRequest request : requests) {
            clone(request.ExistedCaseId, request.CurrentCaseId);
        }
       
    }
    
    global static void clone(String existedCaseId, String currentCaseId){
        
        List<EmailMessage> ExistedEmailList = [SELECT Id, ParentId, ActivityId, TextBody, HtmlBody, Headers, Subject, 
                                               FromName, FromAddress, ToAddress, CcAddress, BccAddress, 
                                               Incoming, HasAttachment, Status, MessageDate, ReplyToEmailMessageId, 
                                               IsExternallyVisible, MessageIdentifier, ThreadIdentifier, IsClientManaged,
                                               RelatedToId, IsTracked, IsOpened, FirstOpenedDate, LastOpenedDate, IsBounced 
                                               FROM EmailMessage 
                                               Where ParentId =:existedCaseId
                                               And Incoming = true];
        
        if(ExistedEmailList != null && ExistedEmailList.size() > 0) {
            
            List<EmailMessage> clonedEmailList = new List<EmailMessage>();
            Map<String, EmailMessage> clonedEmailMap = new Map<String, EmailMessage>();
            
            for(EmailMessage email: ExistedEmailList) {
                EmailMessage clonedEmail = email.clone(); 
                clonedEmail.RelatedToId = currentCaseId;
                clonedEmail.ParentId = currentCaseId;
                
                clonedEmailMap.put(email.Id, clonedEmail);
            }
            
            System.debug('clonedEmailMap.size(): '+clonedEmailMap.size());
            insert clonedEmailMap.values();
            
            System.debug('clonedEmailMap.keySet(): '+clonedEmailMap.keySet());
            List<ContentDocumentLink> contentDocumentLinkList = [Select Id, ContentDocumentId, LinkedEntityId From ContentDocumentLink Where LinkedEntityId In: clonedEmailMap.keySet()];
            if(contentDocumentLinkList != null & contentDocumentLinkList.size() >0) 
            {
                Set<Id> contentDocumentIdSet = new Set<Id>();
                Map<String, String[]> linkedEntityForMap = new Map<String, String[]>();
                for(ContentDocumentLink contentDocumentLink: contentDocumentLinkList) {
                    contentDocumentIdSet.add(contentDocumentLink.ContentDocumentId);
                    
                    String[] contentDocumentArray = linkedEntityForMap.get(contentDocumentLink.LinkedEntityId);
                    if(contentDocumentArray == null){
                        linkedEntityForMap.put(contentDocumentLink.LinkedEntityId, new String[]{contentDocumentLink.ContentDocumentId});
                    }else{
                        contentDocumentArray.add(contentDocumentLink.ContentDocumentId);
                    }
                }
                
                List<ContentVersion> contentList = [Select Id, ContentDocumentId, Title, SharingOption, SharingPrivacy, PathOnClient,
                                                    FileType, PublishStatus, VersionData, FileExtension, FirstPublishLocationId, Origin, ContentLocation
                                                    From ContentVersion 
                                                    Where ContentDocumentId In: contentDocumentIdSet];
                
                if(contentList != null && contentList.size() > 0) {
                    List<ContentVersion> newContentList = new List<ContentVersion>();
                    Map<String, List<ContentVersion>> contentVersionListForMap = new Map<String, List<ContentVersion>>();
                    
                    for(String existedEmailId: clonedEmailMap.keySet())
                    { 
                        String[] contentDocumentArray = linkedEntityForMap.get(existedEmailId);
                        if(contentDocumentArray != null && contentDocumentArray.size()> 0) 
                        {
                            for(ContentVersion contentVersion: contentList)
                            {
                                if(contentDocumentArray.contains(contentVersion.ContentDocumentId)){
                                	List<ContentVersion> contentVersionList = contentVersionListForMap.get(existedEmailId);
                                    if(contentVersionList == null){
                                        contentVersionListForMap.put(existedEmailId, new List<ContentVersion>{contentVersion});
                                    }else {
                                        contentVersionList.add(contentVersion);
                                    }
                                }  
                            }
                            
                        } 
                    }
                    
                    for(String existedEmailId: clonedEmailMap.keySet())
                    {
                        List<ContentVersion> contentVersionList = contentVersionListForMap.get(existedEmailId);
                        if(contentVersionList != null && contentVersionList.size() > 0){
                            for(ContentVersion content: contentVersionList) {
                                
                                ContentVersion newContent = new ContentVersion();
                                newContent.Title = content.Title;
                                newContent.SharingOption = content.SharingOption;
                                newContent.SharingPrivacy = content.SharingPrivacy;
                                newContent.PathOnClient = content.PathOnClient;
                                newContent.VersionData = content.VersionData;
                                newContent.FirstPublishLocationId = clonedEmailMap.get(existedEmailId).Id;
                                newContent.Origin = content.Origin;
                                newContent.ContentLocation = content.ContentLocation;
                                
                                newContentList.add(newContent);
                            }
                        }
                    }
                    
                    
                    
                    
                    if(newContentList != null && newContentList.size() > 0) {
                        insert newContentList;
                        
                        List<ContentVersion> docList = [Select Id, ContentDocumentId, Title, SharingOption, SharingPrivacy, PathOnClient,
                                                              FileType, PublishStatus, VersionData, FileExtension, FirstPublishLocationId, Origin, ContentLocation
                                                              From ContentVersion 
                                                              Where Id In: newContentList];
                        
                        List<ContentDocumentLink> newLinkList = new List<ContentDocumentLink>();
                        //clone all attachment to related file on current case record
                        for(ContentVersion document: docList) {
                            ContentDocumentLink newLink =new ContentDocumentLink();
                            newLink.ContentDocumentId = document.ContentDocumentId;
                            newLink.LinkedEntityId = currentCaseId;
                            newLink.ShareType = 'V';
                            newLink.Visibility = 'AllUsers';
                            
                            newLinkList.add(newLink);
                        }
                        
                        if(newLinkList != null && newLinkList.size() > 0) {
                            insert newLinkList;
                        }
                    } 
                    
                    
                    
                }
                
            }
            
        }
    }
    
    
    global class CaseEmailRequest 
    {
        @InvocableVariable(required=true)
        global ID ExistedCaseId;
    
        @InvocableVariable(required=true)
        global ID CurrentCaseId;
     }
    
    global class CaseEmailResponse
    {
        @InvocableVariable
        global ID clonedEmailMessageId;
        
        @InvocableVariable
        global boolean Success;
    }
    
}