trigger QuoteTrigger on SBQQ__Quote__c (after insert, after update, before delete) {
    
    if(Trigger.IsAfter) {
        if(Trigger.IsInsert) {
            QuoteController.createQuote(Trigger.New);
        }
        
        if(Trigger.IsUpdate) {
            Map<String, SBQQ__Quote__c> mapOldQuote = new Map<String, SBQQ__Quote__c>();
            Map<String, SBQQ__Quote__c> mapNewQuote = new Map<String, SBQQ__Quote__c>();
            List<SBQQ__Quote__c> updQuoteList = new List<SBQQ__Quote__c>();
            
            for(SBQQ__Quote__c quote : Trigger.Old){
                mapOldQuote.put(quote.Id, quote);
            }
            for(SBQQ__Quote__c quote : Trigger.New){
                mapNewQuote.put(quote.Id, quote);
            }
            for(SBQQ__Quote__c quote : Trigger.New){
                SBQQ__Quote__c oldQuote = mapOldQuote.get(quote.Id);
                SBQQ__Quote__c newQuote = mapNewQuote.get(quote.Id);
                if ((oldQuote.SBQQ__Account__c != newQuote.SBQQ__Account__c) ||
                   (oldQuote.SBQQ__Opportunity2__c != newQuote.SBQQ__Opportunity2__c)){
                       updQuoteList.add(newQuote);
                }
            }
            
            if(updQuoteList.size() > 0) {
                QuoteController.updateQuote(updQuoteList);
            }
        }
    }
    
    if(Trigger.IsBefore) {
        if(Trigger.IsDelete) {
            QuoteController.deleteQuote(Trigger.Old);
        }
    }
}